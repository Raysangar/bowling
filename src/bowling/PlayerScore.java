package bowling;

import java.util.Iterator;

public class PlayerScore {
    private final String name;
    private final int[] throwns;
    private int size;

    PlayerScore(String name) {
        this.name = name;
        throwns = new int[21];
        size = 0;
        
    }

    public void addThrow(int pins) {
        throwns[size++] = pins;
    }
    
    Object getPlayerName() {
        return name;
    }

    public int getScore() {
        int score = 0;
        for (Frame frame : frames())
            score += frame.getPoints();
        return score;
    }
    
    public Iterable<Integer> throwns() {
        return createIterableOfThrowns();
    }
    
    public Iterable<Frame> frames() {
        return createIterableOfFrames();
    }

    private Iterable<Integer> createIterableOfThrowns() {
        return new Iterable<Integer>() {

            @Override
            public Iterator<Integer> iterator() {
                return createIteratorOfThrowns();
            }
        };
    }

    private Iterable<Frame> createIterableOfFrames() {
        return new Iterable<Frame>() {

            @Override
            public Iterator<Frame> iterator() {
                return createIteratorOfFrames();
            }
        };
    }
    
    private Iterator<Frame> createIteratorOfFrames() {
        return new Iterator<Frame>() {

            private int index = 0;
            @Override
            public boolean hasNext() {
                return new Frame(index).isCalculable();
            }

            @Override
            public Frame next() {
                Frame frame = new Frame(index);
                index += (frame.isStrike() ? 1 :2);
                return frame;
            }

            @Override
            public void remove() {}
        };
    }
    
    public class Frame {
        private int index;

        public Frame(int index) {
            this.index = index;
        }        
        
        public int getPoints() {
            if (!isCalculable()) return 0;
            return throwns[index] + throwns[index + 1] + (isFull() ? throwns[index + 2] : 0);
        }

        private boolean isCalculable() {
            return index + (isFull() ? 2 : 1) < size;
        }

        private boolean isSpare() {
            return throwns[index] + throwns[index + 1] == 10;
        }

        private boolean isFull() {
            return isStrike() || isSpare();
        }

        private boolean isStrike() {
            return throwns[index] == 10;
        }
    }
    
    private Iterator<Integer> createIteratorOfThrowns() {
        return new Iterator<Integer>() {

            private int index = 0;
            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public Integer next() {
                return throwns[index++];
            }

            @Override
            public void remove() {}
        };
    }
}
