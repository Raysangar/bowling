package bowling;

public class InvalidThrowException extends RuntimeException {

    public InvalidThrowException() {
    }

    public InvalidThrowException(String message) {
        super(message);
    }

}
